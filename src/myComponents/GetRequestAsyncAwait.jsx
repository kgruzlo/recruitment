import { getValueAndUnit } from 'polished';
import React, { useState, useEffect } from 'react';
import Button from '../components/Button';
import Container from '../components/Container';
import H6 from '../components/H6';
import Input from '../components/Input';
import './GetRequestAsyncAwait.css';

const axios = require('axios');

const GetRequestAsyncAwait = () => {
  let [getData, setGetData] = useState({ 'message': 'Loading tournaments ...' });


  useEffect(() => {
    const sendGetRequest = async () => {
      try {
        const resp = await axios.get('http://localhost:4000/tournaments');
        console.log('Status: ' + resp.statusText);
        setGetData(resp.data);
      } catch (err) {
        console.error(err);
        setGetData({ 'message': 'Something went wrong.' });
      }
    };
    sendGetRequest();
  }, []);

  return (
    <article>
      <div className="top-bar">
        <Input placeholder="Search tournament..." />
        <Button>Create tournament</Button>
      </div>
      {getData.message
        ? (
          <div className="test">
            <p>
              {getData.message}
            </p>
            <Button>Retry</Button>
          </div>
        ) : (
          <Container className='test2'>
            {getData.map((velue, key) => <div className='test3' key={key}>
              <div className="test4">
                <H6>
                  {velue.name}
                </H6>
                Organizer: {velue.organizer} <br />
                Game: {velue.game} <br />
                Participants: {velue.participants.current}/{velue.participants.max} <br />
                Start: {velue.startDate}
              </div>
              <div className="test5">
                <Button>Edit</Button>
                <Button>Delete</Button>
              </div>
            </div>)}
          </Container>
        )
      }
    </article>
  );
};

export default GetRequestAsyncAwait;
